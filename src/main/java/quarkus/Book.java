package quarkus;

import org.bson.types.ObjectId;

import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.quarkus.mongodb.panache.common.MongoEntity;

@MongoEntity(collection = "books", database="test")
public class Book extends PanacheMongoEntity {
    
    public ObjectId id;
    public String nameBook;
    public String resumeBook;

    
    public ObjectId getId() {
        return this.id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNameBook() {
        return this.nameBook;
    }

    public void setNameBook(String nameBook) {
        this.nameBook = nameBook;
    }

    public String getResumeBook() {
        return this.resumeBook;
    }

    public void setResumeBook(String resumeBook) {
        this.resumeBook = resumeBook;
    }

}
