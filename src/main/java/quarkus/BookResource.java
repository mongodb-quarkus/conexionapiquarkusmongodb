package quarkus;

import org.bson.types.ObjectId;


import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/books")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BookResource {
    
    @Inject
    BookRepository bookRepository;

    @GET
    public Response getAllBooks() {
        return Response.ok(bookRepository.listAll()).build();
    }

    @GET
    @Path("/{id}")
    public Book getBookById(@PathParam("id")String id){
        ObjectId objectId = new ObjectId(id);
        return bookRepository.findById(objectId);
    }

    @POST
    public Response addBook( Book book) {
        bookRepository.persist(book);
        return Response.status(Response.Status.CREATED).entity(book).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteBook(@PathParam("id") String id) {
        ObjectId objectId = new ObjectId(id);
        Book book = bookRepository.findById(objectId);

        if (book == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        bookRepository.delete(book);
        return Response.noContent().build();
    }


    @PUT
    @Path("/{id}")
    public Response updateBook(@PathParam("id")String id, Book book){
        ObjectId objectId;
        try {
            objectId = new ObjectId(id);
        }catch (IllegalArgumentException e){
            return Response.status(Response.Status.BAD_REQUEST).entity("ERROR no existe").build();
        }

        Book updateBook = bookRepository.findById(objectId);

        if(updateBook != null){
            if(book.getNameBook() != null){
                updateBook.setNameBook(book.getNameBook());
            }
            if(book.getResumeBook()!= null){
                updateBook.setResumeBook(book.getResumeBook());
            }
           
            bookRepository.update(updateBook);
            return Response.ok("Book actualizado").build();
            

        } else {
            return Response.status(Response.Status.NOT_FOUND).entity("Book no encontrado o no existe").build();
        }
    }

}
